package nl.payconiq.stocks.service;

import nl.payconiq.stocks.data.Stock;
import nl.payconiq.stocks.data.StockPrice;
import nl.payconiq.stocks.utils.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StockServiceImp implements StockService {

    Map<Long, Stock> data;

    StockServiceImp() {
        Stock s = new Stock(1L, "some name",
                Stream.of(new StockPrice(120, Utils.getCurrentTimestamp())).collect(Collectors.toList()));
        this.data = new HashMap<>();
        this.data.put(s.getId(), s);
    }

    @Override
    public List<Stock> getStocks() {
        return new ArrayList<>(data.values());
    }

    @Override
    public Stock getStockById(long id) {
        return data.get(id);
    }

    @Override
    public List<StockPrice> getStockPriceHistoryByStockId(long id) {
        if (data.containsKey(id)) {
            return data.get(id).getStockPriceList();
        }
        return new ArrayList<>();
    }

    @Override
    public Stock updateStockPrice(long id, double newPrice) {
        if (data.containsKey(id)) {
            StockPrice stockPrice = new StockPrice(newPrice, Utils.getCurrentTimestamp());
            data.get(id).getStockPriceList().add(0, stockPrice);
            return data.get(id);
        }
        return null;
    }

    @Override
    public Stock createStock(Stock stock) {
        stock.setId(Utils.getCurrentTimestamp());
        data.put(stock.getId(), stock);
        return stock;
    }


}
