package nl.payconiq.stocks.service;

import nl.payconiq.stocks.data.Stock;
import nl.payconiq.stocks.data.StockPrice;

import java.util.List;

public interface StockService {


    List<Stock> getStocks();
    Stock getStockById(long id);
    List<StockPrice> getStockPriceHistoryByStockId(long id);
    Stock updateStockPrice(long id, double newPrice);
    Stock createStock(Stock stock);
}
