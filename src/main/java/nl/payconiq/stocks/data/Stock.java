package nl.payconiq.stocks.data;

import java.util.List;
import java.util.Objects;

public class Stock {
    private long id;
    private String name;
    private List<StockPrice> stockPriceList;

    public Stock() {
    }

    public Stock(long id, String name, List<StockPrice> stockPriceList) {
        this.id = id;
        this.name = name;
        this.stockPriceList = stockPriceList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StockPrice> getStockPriceList() {
        return stockPriceList;
    }

    public void setStockPriceList(List<StockPrice> stockPriceList) {
        this.stockPriceList = stockPriceList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return id == stock.id &&
                Objects.equals(name, stock.name) &&
                Objects.equals(stockPriceList, stock.stockPriceList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, stockPriceList);
    }
}