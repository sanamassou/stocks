package nl.payconiq.stocks.data;

import java.util.Date;
import java.util.Objects;

public class StockPrice {
    private double currentPrice;
    private long lastUpdate;

    public StockPrice(double currentPrice, long lastUpdate) {
        this.currentPrice = currentPrice;
        this.lastUpdate = lastUpdate;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockPrice that = (StockPrice) o;
        return Double.compare(that.currentPrice, currentPrice) == 0 &&
                lastUpdate == that.lastUpdate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(currentPrice, lastUpdate);
    }
}
