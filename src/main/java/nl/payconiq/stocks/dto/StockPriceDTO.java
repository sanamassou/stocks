package nl.payconiq.stocks.dto;

import nl.payconiq.stocks.data.StockPrice;

import java.util.List;
import java.util.stream.Collectors;

public class StockPriceDTO {
    private double price;
    private long lastUpdate;

    public StockPriceDTO(double price, long lastUpdate) {
        this.price = price;
        this.lastUpdate = lastUpdate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double newPrice) {
        this.price = newPrice;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public static StockPriceDTO fromEntity(StockPrice entity) {
        return new StockPriceDTO(entity.getCurrentPrice(), entity.getLastUpdate());
    }

    public static List<StockPriceDTO> fromEntity(List<StockPrice> entities) {
        return entities.stream().map(StockPriceDTO::fromEntity).collect(Collectors.toList());
    }
}
