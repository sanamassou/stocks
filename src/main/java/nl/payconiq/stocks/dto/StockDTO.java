package nl.payconiq.stocks.dto;

import nl.payconiq.stocks.data.Stock;
import nl.payconiq.stocks.data.StockPrice;
import nl.payconiq.stocks.utils.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StockDTO {

    private long id;
    private String name;
    private double price;
    private long lastUpdate;

    public StockDTO(long id, String name, double price, long lastUpdate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.lastUpdate = lastUpdate;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public static StockDTO fromEntity(Stock stock) {
        StockPrice stockPrice = stock.getStockPriceList().get(0);
        return new StockDTO(stock.getId(), stock.getName(), stockPrice.getCurrentPrice(), stockPrice.getLastUpdate());
    }

    public static List<StockDTO> fromEntity(List<Stock> stocks) {
        return stocks.stream().map(StockDTO::fromEntity).collect(Collectors.toList());

    }

    public static Stock toEntity(StockDTO stockDTO) {
        Stock stock = new Stock();
        stock.setName(stockDTO.getName());
        StockPrice stockPrice = new StockPrice(stockDTO.getPrice(), Utils.getCurrentTimestamp());
        stock.setStockPriceList(Stream.of(stockPrice).collect(Collectors.toList()));
        return stock;
    }
}
