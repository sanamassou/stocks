package nl.payconiq.stocks.utils;

import java.time.ZonedDateTime;

public interface Utils {

    static long getCurrentTimestamp() {
        return ZonedDateTime.now().toInstant().toEpochMilli();
    }
}
