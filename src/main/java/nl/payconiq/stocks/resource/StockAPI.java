package nl.payconiq.stocks.resource;

import nl.payconiq.stocks.dto.StockDTO;
import nl.payconiq.stocks.dto.StockPriceDTO;
import nl.payconiq.stocks.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/stocks")
public class StockAPI {
    private StockService stockService;

    @Autowired
    StockAPI(StockService stockService){
        this.stockService = stockService;
    }

    @GetMapping
    List<StockDTO> getStocks(){
        return StockDTO.fromEntity(stockService.getStocks());
    }

    @GetMapping("{id}")
    StockDTO getStockById(@PathVariable("id") long id) {
        return StockDTO.fromEntity(stockService.getStockById(id));
    }

    @GetMapping("{id}/history")
    List<StockPriceDTO> getStockPriceHistoryByStockId(@PathVariable("id") long id) {
        return StockPriceDTO.fromEntity(stockService.getStockPriceHistoryByStockId(id));
    }

    @PutMapping("{id}")
    StockDTO updateStockPrice(@PathVariable("id") long id, @RequestBody StockPriceDTO stockPriceDto) {
        return StockDTO.fromEntity(stockService.updateStockPrice(id, stockPriceDto.getPrice()));
    }

    @PostMapping
    StockDTO createStock(@RequestBody StockDTO stockDto) {
        return StockDTO.fromEntity(stockService.createStock(StockDTO.toEntity(stockDto)));
    }

}
