package nl.payconiq.stocks.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import nl.payconiq.stocks.data.Stock;
import nl.payconiq.stocks.data.StockPrice;
import nl.payconiq.stocks.dto.StockDTO;
import nl.payconiq.stocks.dto.StockPriceDTO;
import nl.payconiq.stocks.service.StockService;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
class StockAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockService service;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void getStocks() throws Exception {

        long fixedTimestamp = 112233l;
        List<Stock> expectedStocks = Collections.singletonList(new Stock(1L, "some name",
                Stream.of(new StockPrice(120, fixedTimestamp)).collect(Collectors.toList()))
        );
        List<StockDTO> expectedStockDtoList = StockDTO.fromEntity(expectedStocks);

        when(service.getStocks()).thenReturn(expectedStocks);

        mockMvc.perform(get("/api/stocks"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedStockDtoList)));
    }

    @Test
    void getStockById() throws Exception {
        long fixedTimestamp = 112233l;
        Stock expectedStock = new Stock(1L, "some name",
                Stream.of(new StockPrice(120, fixedTimestamp)).collect(Collectors.toList()));
        StockDTO expectedStockDto = StockDTO.fromEntity(expectedStock);
        when(service.getStockById(1L)).thenReturn(expectedStock);
        mockMvc.perform(get("/api/stocks/" + expectedStock.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedStockDto)));

    }

    @Test
    void getStockPriceHistoryByStockId() throws Exception {
        long fixedTimestamp = 112233l;
        Stock expectedStock = new Stock(1L, "some name",
                Stream.of(new StockPrice(120, fixedTimestamp)).collect(Collectors.toList()));
        List<StockPriceDTO> expectedStockPriceDtoList = StockPriceDTO.fromEntity(expectedStock.getStockPriceList());
        when(service.getStockPriceHistoryByStockId(1L)).thenReturn(expectedStock.getStockPriceList());
        mockMvc.perform(get("/api/stocks/" + expectedStock.getId() + "/history"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedStockPriceDtoList)));
    }

    @Test
    void updateStockPrice() throws Exception{
        long fixedTimestamp = 112233l;
        StockPriceDTO newStockPrice= new StockPriceDTO(300, fixedTimestamp);
        Stock expectedStock = new Stock(1L, "some name",
                Stream.of(new StockPrice(120, fixedTimestamp),
                        new StockPrice(newStockPrice.getPrice(), newStockPrice.getLastUpdate())).collect(Collectors.toList()));

        when(service.updateStockPrice(expectedStock.getId(),newStockPrice.getPrice())).thenReturn(expectedStock);
        StockDTO expectedStockDto = StockDTO.fromEntity(service.updateStockPrice(expectedStock.getId(), newStockPrice.getPrice()));
        mockMvc.perform(put("/api/stocks/" + expectedStock.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(newStockPrice)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(expectedStockDto)));

    }

    @Test
    void createStock() throws Exception {
        long fixedTimestamp = 112233l;
        Stock expectedStock = new Stock(1L, "some name",
                Stream.of(new StockPrice(120, fixedTimestamp)).collect(Collectors.toList()));
        StockDTO stockDto = StockDTO.fromEntity(expectedStock);

        when(service.createStock(any())).thenReturn(expectedStock);

        mockMvc.perform(post("/api/stocks/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(stockDto)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(stockDto)));



    }
}