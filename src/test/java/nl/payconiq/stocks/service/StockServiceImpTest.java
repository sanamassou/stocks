package nl.payconiq.stocks.service;

import nl.payconiq.stocks.data.Stock;
import nl.payconiq.stocks.data.StockPrice;
import nl.payconiq.stocks.utils.Utils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
class StockServiceImpTest {
    StockService service = new StockServiceImp();
    long fixedTimestamp = 112233l;
    List<Stock> expectedStocks = Collections.singletonList(new Stock(1L, "some name",
            Stream.of(new StockPrice(120, fixedTimestamp)).collect(Collectors.toList()))
    );

    @Test
    void getStocks() {
        List<Stock> actualStocks = service.getStocks();
        assertNotNull(actualStocks);
        setLastUpdatedTimeToFixedValue(actualStocks);
        assertEquals(expectedStocks.size(), actualStocks.size());
        assertEquals(expectedStocks, actualStocks);
    }

    @Test
    void getStockById() {
        Stock actualStock = service.getStockById(1l);
        assertNotNull(actualStock);
        setLastUpdatedTimeToFixedValue(actualStock);
        assertEquals(expectedStocks.get(0), actualStock);
    }

    @Test
    void getStockPriceHistoryByStockId() {
        assertNotNull(service.getStockPriceHistoryByStockId(1L));
    }

    @Test
    void updateStockPrice() {
        assertEquals(2,service.updateStockPrice(1L, 300).getStockPriceList().size());
    }

    @Test
    void createStock() {
        Stock newStock = new Stock(2L, "another name",
                Stream.of(new StockPrice(300, Utils.getCurrentTimestamp())).collect(Collectors.toList()));
        Stock actualStock = service.createStock(newStock);
        newStock.setId(actualStock.getId());
        assertEquals(2, service.getStocks().size());
        assertEquals(newStock,actualStock );
    }

    private  void setLastUpdatedTimeToFixedValue(Stock stock){
        stock.getStockPriceList().forEach(sp -> sp.setLastUpdate(fixedTimestamp));
    }

    private  void setLastUpdatedTimeToFixedValue(List<Stock> stocks){
        stocks.forEach(s -> setLastUpdatedTimeToFixedValue(s));
    }

}